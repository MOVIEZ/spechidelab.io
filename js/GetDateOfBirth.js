var MyDOB = function(year, month, date, hour, minute, second) {
  var yob = new Date(year, month - 1, date, hour, minute, second);
  var todaydate = new Date();
  var BirthYear = year;
  var CurrentYear = todaydate.getFullYear();
  var LeapYearCount = 0;
  for (var i = BirthYear; i <= CurrentYear; i++) {
    if (isLeapYear(i)) {
      LeapYearCount++;
    }
  }
  var differentdates = todaydate - yob;
  var one_sec = 1000;
  var one_min = 60;
  var one_hour = 60;
  var one_day = 24;
  var one_year = 365;
  var x = differentdates / one_sec;
  var seconds = Math.floor(x % one_min);
  x = x / one_min;
  var minutes = Math.floor(x % one_hour);
  x = x / one_hour;
  var hours = Math.floor(x % one_day);
  x = (x / one_day) - LeapYearCount;
  var days = Math.floor(x % one_year);
  x = x / one_year;
  var years = Math.floor(x);
  return {
    'y': years,
    'd': days,
    'H': hours,
    'M': minutes,
    'S': seconds
  };
};
