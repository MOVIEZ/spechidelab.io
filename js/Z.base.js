// Easter Eggs!
$(window).bind('keydown', function(event) {
  // http://stackoverflow.com/questions/93695/best-cross-browser-method-to-capture-ctrls-with-jquery
  if (event.ctrlKey || event.metaKey) {
    switch (String.fromCharCode(event.which).toLowerCase()) {
      case 's':
        event.preventDefault();
        DisplayMessageInModal(
          "Please do not plagiarize this work!",
          "You may take the source code of this website from <a href='https://gitlab.com/spechide/spechide.gitlab.io'>here</a>."
        );
        break;
      case 'f':
        event.preventDefault();
        window.location.href = "//spechide.shrimadhavuk.me/resources/projects.php";
        break;
      case 'm':
        event.preventDefault();
        window.location.href = "//spechide.shrimadhavuk.me/functions/send-mail.php";
        break;
      case 'e':
        event.preventDefault();
        window.location.href = "//spechide.shrimadhavuk.me/resources/timeline.php";
        break;
      case 'a':
        event.preventDefault();
        DisplayMessageInModal(
          "What is this?",
          "This website is an attempt to make my online presence stronger! The idea is inspired from <a href='http://webkay.robinlinus.com/'>What every Browser knows about you</a>."
        );
        break;
    }
  } else {
    switch (event.which) {
      // => http://stackoverflow.com/a/5597114/4723940
      case 37:
        event.preventDefault();
        // LEFT arrow key
        console.log("LEFT: horizontal scrolling disabled");
        break;
      case 38:
        //event.preventDefault();
        //alert("up");
        break;
      case 39:
        event.preventDefault();
        // RIGHT arrow key
        console.log("RIGHT: horizontal scrolling disabled");
        break;
      case 40:
        //event.preventDefault();
        //alert("down");
        break;
        // => http://stackoverflow.com/a/6011119/4723940
    }
  }
  // http://stackoverflow.com/questions/93695/best-cross-browser-method-to-capture-ctrls-with-jquery
});

// => update age
setInterval(function() {
  if (document.getElementById('time_countdown')) {
    var countup = MyDOB(1995, 11, 16, 19, 30, 30);
    var years = countup.y;
    var days = countup.d;
    var hours = countup.H;
    var minutes = countup.M;
    var seconds = countup.S;
    if(days == 0){
      document.getElementById('time_countdown').style = "display: none;";
    }
    else{
      document.getElementById('time_countdown').style = "display: block;";

      document.getElementById('yrsl').innerHTML = "<span class='digit'>" + years.toString().split('').join('</span><span class="digit">') + "</span>";
      document.getElementById('daysl').innerHTML = "<span class='digit'>" + days.toString().split('').join('</span><span class="digit">') + "</span>";
      document.getElementById('hrsl').innerHTML = "<span class='digit'>" + hours.toString().split('').join('</span><span class="digit">') + "</span>";
      document.getElementById('minl').innerHTML = "<span class='digit'>" + minutes.toString().split('').join('</span><span class="digit">') + "</span>";
      document.getElementById('secl').innerHTML = "<span class='digit'>" + seconds.toString().split('').join('</span><span class="digit">') + "</span>";
    }
  }
}, 1000);

// => TTS
var shortname = createsoundbite("https://spechide.shrimadhavuk.me/img/voices/SpEcHiDe_ProNounCiaTion.ogg");
var fullname = createsoundbite("https://spechide.shrimadhavuk.me/img/voices/ShrimadhavUK_ProNounCiaTion.ogg");
