//Frederick, 060605 Add function to close window without warning{
function closeWindow() {
  var currBrowser;
  currBrowser = GetBrowserOS();
  switch (currBrowser) {
    case "msiewin": //ie windows
    case "msiemac": //ie mac
    case "netslin": //mozilla linux
      window.opener = self;
      window.close();
      break;
    case "netswin": //netscape windows
    case "firelin": //firefox linux
    case "firewin": //firefox windows
    case "firemac": //firefox mac
      window.open('', '_parent', '');
      window.close();
      break;
    default:
      window.opener = self;
      window.close();
      break;
  }
}
//Frederick, 060605}

function op() {}
