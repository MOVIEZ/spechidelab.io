var isLeapYear = function(year) {
  // => http://stackoverflow.com/a/16353241
  return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
};
