var isValidDate = function(year, month, day) {
  var f;
  if (month == 2) {
    if (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)) {
      if (day > 29) {
        f = false;
      }
    } else {
      if (day > 28) {
        f = false;
      }
    }
  } else if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
    if (day > 30) {
      f = false;
    }
  } else {
    f = true;
  }
  return f;
};
