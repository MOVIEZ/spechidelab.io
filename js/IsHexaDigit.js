// Checks whether a digit is a valid hexadecimal digit!
var isHexaDigit = function(digit) {
  var hexVals = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "a", "b", "c", "d", "e", "f");
  var len = hexVals.length;
  var i = 0;
  var ret = false;
  for (i = 0; i < len; i++) {
    if (digit == hexVals[i]) {
      break;
    }
  }
  if (i < len) {
    ret = true;
  }
  return ret;
};

// Checks whether a number (val) is a valid HexaDecimal number!
// val == HexaDecimal Number
// size = Length of val
var isValidHexKey = function(val, size) {
  var ret = false;
  if (val.length == size) {
    for (i = 0; i < val.length; i++) {
      if (isHexaDigit(val.charAt(i)) === false) {
        break;
      }
    }
    if (i == val.length) {
      ret = true;
    }
  }
  return ret;
};
