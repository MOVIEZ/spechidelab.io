var html5_audiotypes = {
  mp3: "audio/mpeg",
  mp4: "audio/mp4",
  ogg: "audio/ogg",
  wav: "audio/wav"
};
var createsoundbite = function(d) {
  var b = document.createElement("audio");
  if (b.canPlayType) {
    for (var c = 0; c < arguments.length; c++) {
      var a = document.createElement("source");
      a.setAttribute("src", arguments[c]);
      if (arguments[c].match(/\.(\w+)$/i)) {
        a.setAttribute("type", html5_audiotypes[RegExp.$1]);
      }
      b.appendChild(a);
    }
    b.load();
    b.playclip = function() {
      b.pause();
      b.currentTime = 0;
      b.play();
    };
    return b;
  } else {
    return {
      playclip: function() {
        throw new Error("Your browser doesn't support HTML5 audio unfortunately");
      }
    };
  }
};
