var BLANK_VALID = true;
var BLANK_INVALID = false;
var SPACE_VALID = true;
var SPACE_INVALID = false;
var SLASH_VALID = true;
var SLASH_INVALID = false;

//Add is invalid message
//Function Name: alertInvalid(fieldname, fieldvalue [,additional])
//Description: alerts invalid message containing fieldname and value
//Parameters: fieldname, fieldvalue, additional - Any additional comments to be added
//Output: MessageBox(invalid message)
function alertInvalid(fieldname, fieldvalue, additional) {
  if (additional === undefined)
    alert(fieldname + " " + fieldvalue + " is invalid.");
  else
    alert(fieldname + " " + fieldvalue + " is invalid, " + additional + ".");
}

//Function Name: isValidEmail(fieldvalue,[fieldname]))
//Description: Check if email address entered is valid
//Parameters: fieldvalue, fieldname
//Output: true - no error
//		  false - error
function isValidEmail(fieldvalue, fieldname) {
  var hasField = false;
  if (fieldname !== undefined) hasField = true;

  if (!isValidName(fieldvalue, fieldname, BLANK_INVALID, SPACE_INVALID)) return false;

  var tmpIndex = fieldvalue.indexOf('@');
  var dotIndex = fieldvalue.indexOf('.');

  if ((tmpIndex == -1) || (dotIndex == -1) || (tmpIndex == fieldvalue.length - 1) || (dotIndex == fieldvalue.length - 1) ||
    (tmpIndex === 0) || (dotIndex === 0)) {
    if (hasField) alertInvalid(fieldname, fieldvalue);
    return false;
  }

  return true;
}

// Check if a name valid
//Frederick,060731	add fields and modify error checks
//check if the name is URL friendly
//Function Name: isValidName(name[,fieldname][,isblankvalid][,isSpaceValid])
//Description: Check that name contains no unnecessary characters
//Parameters: name, fieldname(optional): show error message when error encountered
//			isblankvalid: BLANK_VALID - allow empty values	| BLANK_INVALID(default) - don't allow empty values
//			isSpaceValid: SPACE_VALID - allow space characetrs | SPACE_INVALID(default) - don't allow space characters
//output: true:no error		false: error
function isValidName(name, fieldname, isblankvalid, isSpaceValid, isSlashValid) {
  var i = 0;
  var hasField = false;

  if (fieldname !== undefined) hasField = true;

  if (name === "")
    if ((isblankvalid === undefined) || (isblankvalid === false)) {
      if (hasField) alertInvalid(fieldname, name);
      return false;
    }

  if ((isSpaceValid === undefined) || (isSpaceValid === false)) {
    for (i = 0; i < name.length; i++) {
      if (isNameUnsafe(name.charAt(i)) === true) {
        if (hasField) alertInvalid(fieldname, name);
        return false;
      }
    }
  } else {
    for (i = 0; i < name.length; i++) {
      if (isCharUnsafe(name.charAt(i)) === true) {
        if (hasField) alertInvalid(fieldname, name);
        return false;
      }
    }
  }

  if (isSlashValid == SLASH_INVALID) {
    if (name.indexOf("/") != -1) {
      if (hasField) alertInvalid(fieldname, name);
      return false;
    }
  }

  return true;
}


function isNameUnsafe(compareChar) {
  // Jerry 20040628, @ . is allow
  //   var unsafeString = "\"<>%\\^[]`\+\$\,='#&@.: \t";
  var unsafeString = "\"<>%\\^[]`\+\$\,='#&: \t;";

  if (unsafeString.indexOf(compareChar) == -1 && compareChar.charCodeAt(0) > 32 &&
    compareChar.charCodeAt(0) < 123)
    return false; // found no unsafe chars, return false
  else
    return true;
}

// same as is isNameUnsafe but allow spaces
function isCharUnsafe(compareChar) {
  var unsafeString = "\"<>%\\^[]`\+\$\,='#&@.:\t;";

  if (unsafeString.indexOf(compareChar) == -1 && compareChar.charCodeAt(0) >= 32 &&
    compareChar.charCodeAt(0) < 123)
    return false; // found no unsafe chars, return false
  else
    return true;
}
