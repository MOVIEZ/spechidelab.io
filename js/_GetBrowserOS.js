function do_checkstr(string) {
  var detect = navigator.userAgent.toLowerCase();
  place = detect.indexOf(string) + 1;
  thestring = string;
  return place;
}

//Function Name: GetBrowserOS
//Description: Gets the current OS version and browser version of OS
//Parameters: none
//Output: <browser><OS>
function GetBrowserOS() {
  var detect = navigator.userAgent.toLowerCase();
  var OS, browser, version, total, thestring, browseVer;

  if (do_checkstr('konqueror')) {
    browser = "Konqueror";
    OS = "Linux";
  } else if (do_checkstr('safari')) browser = "safa";
  else if (do_checkstr('omniweb')) browser = "omni";
  else if (do_checkstr('opera')) browser = "oper";
  else if (do_checkstr('webtv')) browser = "webt";
  else if (do_checkstr('icab')) browser = "icab";
  else if (do_checkstr('msie')) browser = "msie";
  //Frederick,060721	Add firefox detection
  else if (navigator.userAgent.indexOf("Firefox") != -1) {
    var versionindex = navigator.userAgent.indexOf("Firefox") + 8;
    if (parseInt(navigator.userAgent.charAt(versionindex)) >= 1)
      browser = "fire";
  } else if (!do_checkstr('compatible')) {
    browser = "nets";
  } else browser = "unknown";

  if (browser != "unknown")
    if (!OS) {
      if (do_checkstr('linux')) OS = "lin";
      else if (do_checkstr('x11')) OS = "uni";
      else if (do_checkstr('mac')) OS = "mac";
      else if (do_checkstr('win')) OS = "win";
      else OS = "unknown";
    }

  browseVer = browser + OS;

  return browseVer;
}
